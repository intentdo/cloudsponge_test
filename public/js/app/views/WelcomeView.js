define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/welcome', 'cloudsponge'],
    function(App, Backbone, Marionette, $, Model, template, cloudsponge) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            ui: {
              'contactList': '#contact_list'
            },

            template: template,
            model: new Model({
                mobile: App.mobile
            }),

            // View Event Handlers
            events: {
            },

            initialize: function (options) {
              var that = this;

              cloudsponge.init({
                domain_key: '729aa6370cbc043be55b1667974f750d11e07754',

                //textarea: 'contact_list',

                afterSubmitContacts: that.populateTextarea,

                skipSourceMenu:true,

                afterInit:function() {
                  console.log('cloudsponge initialized');
                  var i, links = document.getElementsByClassName('delayed');
                  for(i = 0; i < links.length; i++) {
                    // make the links that launch a popup clickable by setting the href property
                    links[i].href = "#";
                  }
                }
              });
            },

            populateTextarea: function (contacts, source, owner) {
              var owner_email, owner_first_name, owner_last_name;
              var appendInTextarea = true;  // whether to append to existing contacts in the textarea
              var emailSep = ", ";  // email address separator to use in textarea

              var contact, name, email, entry;
              var emails = [];
              var textarea = document.getElementById('contact_list');
              //var textarea = this.ui.contactList;

              // preserve the original value in the textarea
              if (appendInTextarea && textarea.value.strip().length > 0) {
                emails = textarea.value.split(emailSep);
              }

              // format each email address properly
              for (var i = 0; i < contacts.length; i++) {
                contact = contacts[i];
                name = contact.fullName();
                email = contact.selectedEmail();
                entry = name + "<" + email +">";
                if (emails.indexOf(entry) < 0) {
                  emails.push(entry);
                }
              }
              // dump everything into the textarea
              textarea.value = emails.join(emailSep);

              // capture the owner information
              owner_email = (owner && owner.email && owner.email[0] && owner.email[0].address) || "";
              owner_first_name = (owner && owner.first_name) || "";
              owner_last_name = (owner && owner.last_name) || "";
            }
        });
      });
